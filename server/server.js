import express from "express";
import http from "http";
import path from "path";
import { PORT } from "./config.js";
import fs from 'fs';
import bodyParser from "body-parser";
import cors from "cors";

const app = express();
const __dirname = path.resolve();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const messages = JSON.parse(fs.readFileSync(path.join(__dirname, 'server/dbase/messages.json'), 'utf8'));
let users = JSON.parse(fs.readFileSync(path.join(__dirname, 'server/dbase/users.json'), 'utf8'));

const httpServer = http.Server(app);

app.get("/messages", (req, res) => {
  res.json(messages);
});

app.get("/users", (req, res) => {
  res.json(users);
});

app.delete("/users/:id", (req, res) => {
  users.users = users.users.filter((it) => it.id !== req.params.id);
  fs.writeFileSync(path.join(__dirname, 'server/dbase/users.json'), JSON.stringify(users)); 
  res.status(200).send(req.params.id);
});

app.post("/users", (req, res) => {
  
  res.status(200).send('req.params.id');
});

httpServer.listen(PORT, () => {
  console.log(`Express: Listen server on port ${PORT}`);
});