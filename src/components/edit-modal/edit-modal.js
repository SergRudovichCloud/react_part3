import React, { useState } from 'react';
import './edit-modal.scss';
import { hideModal, editMessage } from 'store/actions';
import { useDispatch, useSelector } from 'react-redux';

const EditModal = () => {

    const [value, setValue] = useState('');

    const { editModal, message } = useSelector((state) => ({
        editModal: state.chatList.chat.editModal,
        message: state.chatList.chat.editMessage
    }));

    const dispatch = useDispatch();

    const onSave = () => {
        let editedMessage = {...message};
        editedMessage.text = value;
        editedMessage.editedAt = Date.now();
        setValue('');
        dispatch(editMessage(editedMessage));
        dispatch(hideModal());
    }

    const getModalContent = () => {
        if (value === '') setValue(message.text);
        return (
            <div className="edit-message-modal modal-shown">
                <div className="modal-header">
                    <button className="edit-message-close" onClick={() => dispatch(hideModal())}>x</button>
                </div>
                <div className="edit-message-wrapper">
                    <textarea
                        className="edit-message-input"
                        value={value}
                        onChange={(event) => {
                            setValue(event.target.value);
                        }}
                    ></textarea>
                    <button
                        className="edit-message-button"
                        onClick={onSave}
                    >Save</button>
                </div>
            </div>
        )
    }

    return editModal ? getModalContent() : null
}

export default EditModal;