import React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import "./header.scss";

const Header = () => {
    const { usersCount, messagesCount, lastMessageData } = useSelector((state) => ({
        usersCount: state.chatList.chat.usersCount,
        messagesCount: state.chatList.chat.messagesCount,
        lastMessageData: state.chatList.chat.lastMessageData,
    }));
    return (
        <div className="header">
            <div className="header-title">My Chat</div>
            <div>
                <span className="header-users-count">{usersCount}</span><span> users</span>
            </div>
            <div>
                <span className="header-messages-count">{messagesCount}</span>
                <span> messages</span>
            </div>
            <div>
                <span>Last message at </span>
                <span className="header-last-message-date">{moment(lastMessageData).format('DD.MM.YYYY HH:mm')}</span>
            </div>
        </div>
    )
}

export default Header;