import React from 'react';
import moment from 'moment';
import './message_item.scss';

const MessageItem = (props) => {
    return (
        <div>
            <div className="message-time">{moment(props.message.createdAt).format('HH:mm')}</div>
            <div className="message-text">{props.message.text}</div>
        </div>
    )
}

export default MessageItem;