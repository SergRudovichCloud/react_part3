import React from 'react';
import './user-add.scss';
import { useFormField } from 'hooks/hooks';
import { useDispatch } from 'react-redux';
import { addUser } from 'store/actions';

const UserAddForm = (props) => {
    const dispatch = useDispatch();

    const emailField = useFormField();
    const usernameField = useFormField();
    const passwordField = useFormField();

    const handleAddUser = (e) => {
        e.preventDefault();
        dispatch(addUser({
            id: Date.now(),
            username:usernameField.value,
            email: emailField.value,
            password: passwordField.value
        }));
        props.setIsUserAdd(false);
    };

    const handleCansel = (e) => {
        e.preventDefault();
        props.setIsUserAdd(false);
    };

    return (
        <div className="user-add flex-center">
            <div className="user-add-form">
                <div>
                    <label htmlFor='email'>Email</label>
                    <input type='text' id='email' {...emailField} placeholder="email" />
                </div>
                <div>
                    <label>Login</label>
                    <input type='text' id='login' {...usernameField} placeholder="username" />
                </div>
                <div>
                    <label htmlFor='password'>Password</label>
                    <input type='password' id='password' {...passwordField} placeholder="password" />
                </div>
                <button onClick={handleAddUser}>Add user</button>
                <button onClick={handleCansel}>Cansel</button>
            </div>
        </div>

    );
}

export default UserAddForm;