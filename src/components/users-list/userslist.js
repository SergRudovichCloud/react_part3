import React, { useState } from 'react';
import UserItem from './user-item/userItem';
import UserAddForm from './user-add/user-add';
import './userslist.scss';
import { useSelector, useDispatch } from 'react-redux';
import { loadUsers } from 'store/actions';
import { Menu } from '../index';

const UsersList = () => {
	const [isUserAdd, setIsUserAdd] = useState(false);
	const dispatch = useDispatch();


	const handlAddUser = () => {
		setIsUserAdd(true);
	};

	const { isAdmin, users } = useSelector((state) => ({
		isAdmin: state.chatList.chat.isAdmin,
		users: state.chatList.chat.users,
	}));
	if (isAdmin) {
		if (!isUserAdd) {
			return (
				<div>
					<Menu />
					<div className="user-list">
						<button onClick={handlAddUser}>Add user</button>
						{users.map((user) => {
							return <UserItem key={user.id} user={user} />;
						})}
					</div>
				</div>
			);
		} else {
			return (
				<div>
					<Menu />
					<div className="user-add">
					<UserAddForm 
					   setIsUserAdd={setIsUserAdd}
					/>
					</div>
				</div>
			);
		}
	} else {
		return (
			<div className="flex-center warning">
				<h1>You don`t have enough permitions</h1>
			</div>
		);
	}
};

export default UsersList;
