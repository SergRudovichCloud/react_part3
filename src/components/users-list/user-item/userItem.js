import React from 'react';
import './user-item.scss';
import { useDispatch } from 'react-redux';
import { deleteUser, loadUsers } from 'store/actions';

const UserItem = (props) => {
    const dispatch = useDispatch();
    const handleDelete = () => {
        dispatch(deleteUser(props.user));
        dispatch(loadUsers());
    }
    return (
        <div className="user-item">
            <span>{props.user.username}</span>
            <span>{props.user.email}</span>
            <div className="user-controls">
                <button>Edit</button>
                <button onClick={handleDelete}>Delete</button>
            </div>
        </div>

    )
}

export default UserItem;