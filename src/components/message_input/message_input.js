import React, { useState } from 'react';
import "./message_input.scss";
import { sendMessage } from 'store/actions';
import { useDispatch } from 'react-redux';

const MessageInput = () => {

    const [value, setValue] = useState('');
    const dispatch = useDispatch();

    const handleSend = () => {
        setValue('');
        dispatch(sendMessage(value)) 
    }
        return (
            <div className="message-input">
                <textarea
                    className="message-input-text"
                    value={value}
                    onChange={(event) => {
                        setValue(event.target.value);
                    }}
                ></textarea>
                <button
                    className="message-input-button"
                    onClick={handleSend}
                >Send</button>
            </div>
        )
}

export default MessageInput;