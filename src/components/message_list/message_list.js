import React from 'react';
import { OwnMessage, Message } from '../index';
import { USER_ID } from '../../config';
import './message_list.scss';
import { useSelector } from 'react-redux';

const MessageList = () => {
    const { messages, lastOwnMessageId } = useSelector((state) => ({
        messages: state.chatList.chat.messages,
        lastOwnMessageId: state.chatList.chat.lastOwnMessageId,
    }));

    return (
        <div className="message-list">
            {messages.map(message => {
                if (message.userId === USER_ID) {
                    return <OwnMessage
                        key={message.id}
                        message={message}
                        lastOwnMessageId={lastOwnMessageId}
                    />
                } else {
                    return <Message key={message.id}
                        message={message}
                    />
                }

            })}
        </div>
    )
}

export default MessageList;