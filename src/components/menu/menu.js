import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import "./menu.scss";

const Menu = () => {
    const { isAdmin } = useSelector((state) => ({
        isAdmin: state.chatList.chat.isAdmin,
    }));
    if (isAdmin) {
        return (
            <div className="menu">
                <div className="menu-links">
                    <Link to="/chat">Chat</Link>
                    <Link to="/users">Users</Link>
                </div>
                <div>
                    <span>You are logged as Admin</span>
                </div>
            </div>
        )
    } else {
        return null;
    }

}

export default Menu;