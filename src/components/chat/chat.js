import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadData, loadUsers } from 'store/actions';
import './chat.scss';
import {
    MessageInput,
    MessageList,
    Header,
    EditModal,
    Preloader,
    Menu
} from '../index';

const Chat = (props) => {
    const preloader = useSelector(state => state.chatList.chat.preloader)
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(loadData());
        dispatch(loadUsers());
    });

    if (!preloader) {
        return (
            <div className="chat">
                <div className='logo'>Logo</div>
                <Menu />
                <Header />
                <MessageList />
                <EditModal />
                <MessageInput />
            </div>
        )
    } else {
        return (
            <Preloader />
        )
    }
}

export default Chat;