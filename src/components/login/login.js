import React from 'react';
import './login.scss';
import { useFormField } from 'hooks/hooks';

const Login = () => {
    const usernaameField = useFormField();
    const passwordField = useFormField();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(usernaameField.value, passwordField.value);
        window.location="/chat";
    };
    return (
        <div className="login flex-center">
            <form onSubmit={handleSubmit} className="login-form">
                <div>
                    <label>Login</label>
                    <input type='text' id='login' {...usernaameField} placeholder="username"/>
                </div>
                <div>
                    <label htmlFor='password'>Password</label>
                    <input type='password' id='password' {...passwordField} />
                </div>
                <button>Log in</button>
            </form>
        </div>

    );
}

export default Login;