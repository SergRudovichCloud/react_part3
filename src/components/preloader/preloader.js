import React from 'react';
import './preloader.scss';

const Preloader = () => {
    return (
        <div className="preloader">
            <span>I am loading...</span>
        </div>
    )
}

export default Preloader;