import React, { useState } from 'react';
import { MessageItem } from '../index';
import './message.scss';

const Message = (props) => {
    const [isLike, setIsLike] = useState(false);

    return (
        <div className="message">
            <div>
                <div className="message-user-avatar">
                    <img src={props.message.avatar} alt="avatar" />
                </div>
                <div className="message-user-name">{props.message.user}</div>
            </div>
            <div className="message-body">
                <MessageItem message={props.message} />
                <button
                    className={isLike ? "message-liked" : "message-like"}
                    onClick={() => {
                        isLike ? setIsLike(false) : setIsLike(true);
                    }}
                >Like</button>
            </div>
        </div>
    )
}

export default Message;