import React from 'react';
import { MessageItem } from '../index';
import './own_message.scss';
import { useDispatch } from 'react-redux';
import { showModal, deleteMessage } from 'store/actions';

const OwnMessage = (props) => {

    const dispatch = useDispatch();
    const handleEdit = () => {
        dispatch(showModal(props.message.id));
    }

    const handleDelete = () => {
        dispatch(deleteMessage(props.message.id));
    }

    if (props.lastOwnMessageId === props.message.id) {
        return (
            <div className="own-message">
                <MessageItem message={props.message} />
                <div className="controls">
                    <button
                        className="message-edit"
                        onClick={handleEdit}
                    >Edit</button>
                    <button
                        className="message-delete"
                        onClick={handleDelete}
                    >Delete</button>
                </div>

            </div>
        )
    } else {
        return (
            <div className="own-message">
                <MessageItem message={props.message} />
                <div className="controls">
                    <button
                        className="message-delete"
                        onClick={handleDelete}
                    >Delete</button>
                </div>

            </div>
        )
    }
}

export default OwnMessage;