import { AppPath } from 'common/enums/enums';
import { Route, Switch } from 'react-router-dom';
import { Chat, Login, UsersList, NotFound, MessageEditor, UserEditor } from "components";

const App = () => (
    <>
        <main>
            <Switch>
                <Route path={AppPath.ROOT} exact component={Login} />
                <Route path={AppPath.LOGIN} exact component={Login} />
                <Route path={AppPath.CHAT} exact component={Chat} />
                <Route path={AppPath.USERS} exact component={UsersList} />
                <Route path={AppPath.MESSAGE_EDITOR_$ID} exact component={MessageEditor} />
                <Route path={AppPath.USER_EDITOR_$ID} exact component={UserEditor} />
                <Route path={AppPath.ANY} exact component={NotFound} />
            </Switch>
        </main>
    </>
);

export default App;