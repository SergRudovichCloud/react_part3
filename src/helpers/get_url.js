function getUrl(url, getData) {
    fetch(url)
    .then(response => {
        if (!response.ok) {
            throw new Error("HTTP error " + response.status);
        }
        return response.json();
    })
    .then(json => {
        getData(json);
    })
}

export { getUrl };