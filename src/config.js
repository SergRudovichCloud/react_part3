const USER_ID = "11111";
const USER_NAME = "Kris";
const MESSAGE_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";
const BASE_URL_MESSAGES = 'http://localhost:3001/messages';
const BASE_URL_USERS = 'http://localhost:3001/users';
const BASE_URL = 'http://localhost:3001/';


export { USER_ID, MESSAGE_URL, USER_NAME, BASE_URL_MESSAGES, BASE_URL_USERS, BASE_URL };