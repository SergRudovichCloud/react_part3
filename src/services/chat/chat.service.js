import { ApiPath, ContentType, HttpMethod } from 'common/enums/enums';

class Chat {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.TODOS;
  }

  getAll(path) {
        return this._http.load(this._getUrl(path), {
      method: HttpMethod.GET,
    });
  }

  getOne(id) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.GET,
    });
  }

  create(payload) {
    return this._http.load(this._getUrl(), {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  update(payload) {
    return this._http.load(this._getUrl(payload.id), {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  partialUpdate(id, payload) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  delete(id) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.DELETE,
    });
  }

  _getUrl(path = '') {
    return `${this._baseUrl}/${path}`;
  }
}

export { Chat };
