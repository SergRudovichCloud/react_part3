import { BASE_URL_MESSAGES, BASE_URL_USERS } from 'config';
import { Http } from './http/http.service';
import { Chat } from './chat/chat.service';

const http = new Http();
const chatService = new Chat({
  baseUrl: BASE_URL_MESSAGES,
  http,
});

const userService = new Chat({
  baseUrl: BASE_URL_USERS,
  http,
});

export { http, chatService, userService };
