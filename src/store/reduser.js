import { createReducer } from '@reduxjs/toolkit';
import {
    loadData,
    deleteMessage,
    editMessage,
    sendMessage,
    hideModal,
    showModal,
    loadUsers,
    deleteUser,
    addUser
} from './actions';
import { USER_ID, USER_NAME } from 'config';

const initialState = {
    chat: {
        messages: [],
        users: [],
        editModal: false,
        preloader: true,
        messagesCount: 0,
        usersCount: 0,
        lastMessageData: '2020-07-16T19:48:56.273Z',
        userMesgCount: 0,
        editMessage: null,
        lastOwnMessageId: 0,
        isAdmin: true
    }
}

const chatList = createReducer(initialState, (builder) => {
    builder.addCase(loadData.fulfilled, (state, { payload }) => {
        const { messages } = payload;
        const fetchUsers = new Set();
        messages.messages.forEach(message => {
            fetchUsers.add(message.userId);
        });
        state.chat.messages = [...messages.messages];
        state.chat.preloader = false;
        state.chat.messagesCount = messages.messages.length;
        state.chat.usersCount = fetchUsers.size;
        state.chat.lastMessageData = messages.messages[messages.messages.length - 1]?.createdAt;
    })
    builder.addCase(loadUsers.fulfilled, (state, { payload }) => {
        const { users } = payload;
        state.chat.users = [...users.users];
    })
    builder.addCase(deleteUser.fulfilled, (state, { payload }) => {
        const { user } = payload;
        state.chat.users = state.chat.users.filter((it) => it.id !== user.id);
    });
    // builder.addCase(addUser.fulfilled, (state, { payload }) => {
    //     const { user } = payload;
    //     state.chat.users = state.chat.users.push(user);
    // });
    builder.addCase(deleteMessage, (state, { payload }) => {
        const { id } = payload;
        const result = state.chat.messages.filter(message => message.id !== id);
        let newUser = 0;
        if (state.chat.userMesgCount === 1) newUser = -1;
        state.chat.messages = [...result];
        state.chat.messagesCount = state.chat.messagesCount - 1;
        state.chat.ususersCounters = state.chat.usersCount + newUser;
        state.chat.userMesgCount = state.chat.userMesgCount - 1;
    });
    builder.addCase(editMessage, (state, { payload }) => {
        const { message } = payload;
        const index = state.chat.messages.findIndex(el => el.id === message.id);
        const result = state.chat.messages;
        result[index] = message;
        state.chat.messages = [...result];
    });
    builder.addCase(sendMessage, (state, { payload }) => {
        const { message } = payload;
        const newMessage = {
            id: USER_ID + new Date(),
            userId: USER_ID,
            avatar: "",
            user: USER_NAME,
            text: message,
            createdAt: Date.now(),
            editedAt: ""
        }
        let newUser = 0;
        if (state.chat.userMesgCount === 0) newUser += 1;
        state.chat.messages = [...state.chat.messages, newMessage];
        state.chat.messagesCount = state.chat.messagesCount + 1;
        state.chat.usersCount = state.chat.usersCount + newUser;
        state.chat.lastMessageData = newMessage.createdAt;
        state.chat.userMesgCount = state.chat.userMesgCount + 1;
        state.chat.lastOwnMessageId = newMessage.id;
    });
    builder.addCase(hideModal, (state) => {
        state.chat.editModal = false;
    });
    builder.addCase(showModal, (state, { payload }) => {
        const { id } = payload;
        const message = state.chat.messages.filter(message => message.id === id)
        state.chat.editModal = true;
        state.chat.editMessage = message[0];
    });
});

export { chatList };