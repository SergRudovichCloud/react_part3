import { combineReducers } from "redux"
import chatList from "./reduser"

const rootReducer = combineReducers({
    chatList
})

export default rootReducer;