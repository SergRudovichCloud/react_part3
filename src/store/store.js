import { configureStore } from '@reduxjs/toolkit';
import { chatService, userService } from 'services/services';
import { chatList } from './reduser';

const store = configureStore({
    reducer: {
        chatList
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            thunk: {
                extraArgument: {
                    chatService,
                    userService
                },
            },
            serializableCheck: false,
        })
    },
});

export { store };