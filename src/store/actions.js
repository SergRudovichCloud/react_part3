import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import {
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    SEND_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    LOAD_DATA,
    LOAD_USERS,
    DELETE_USER,
    ADD_USER
} from './actionTypes';

export const loadData = createAsyncThunk(LOAD_DATA, async (_args, { extra }) => ({
    messages: await extra.chatService.getAll(),
}));

export const loadUsers = createAsyncThunk(LOAD_USERS, async (_args, { extra }) => ({
    users: await extra.userService.getAll(),
}));

export const deleteUser = createAsyncThunk(DELETE_USER, async (user, { extra }) => {
    await extra.userService.delete(user.id);

    return {
        user,
    };
});

export const addUser = createAsyncThunk(ADD_USER, async (payload, { extra }) => ({
    users: await extra.userService.create(payload),
  }))

export const deleteMessage = createAction(DELETE_MESSAGE, (id) => ({
    payload: {
        id
    }
}));

export const editMessage = createAction(EDIT_MESSAGE, (message) => ({
    payload: {
        message
    }
}));

export const sendMessage = createAction(SEND_MESSAGE, (message) => ({
    payload: {
        message
    }
}));

export const hideModal = createAction(HIDE_MODAL, () => ({
}));

export const showModal = createAction(SHOW_MODAL, (id) => ({
    payload: {
        id
    }
}));