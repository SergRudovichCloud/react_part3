const AppPath = {
    ROOT: '/',
    CHAT: '/chat',
    USERS: '/users',
    LOGIN: '/login',
    MESSAGE_EDITOR_$ID: '/edit-message/:id',
    USER_EDITOR_$ID: '/edit-user/:id',
    ANY: '*',
  };
  
  export { AppPath };